package de.marcmedia.usercrudproject.controller;

import de.marcmedia.usercrudproject.entities.Project;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @GetMapping("/new")
    public String displayProjectForm(Model model) {
        model.addAttribute("project", new Project());
        return "views/new-project";
    }

    @PostMapping("/save")
    public String createProject() {
        //TODO
        return null;
    }
}
