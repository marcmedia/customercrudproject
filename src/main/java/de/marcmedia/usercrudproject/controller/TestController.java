package de.marcmedia.usercrudproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;

/**
 * @author Marc
 *
 * A Controller just for testing purposes.
 */
@Controller
@RequestMapping("/testing")
public class TestController {

    @GetMapping("/form")
    public String getForm() {
        return "views/form";
    }

    @PostMapping("/test")
    public String testing(Model model, @RequestParam String firstname, @RequestParam String lastname) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("firstname", firstname);
        userData.put("lastname", lastname);
        model.addAllAttributes(userData);
        return "views/test";
    }
}
