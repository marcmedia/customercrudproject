package de.marcmedia.usercrudproject.controller;

import de.marcmedia.usercrudproject.entities.Customer;
import de.marcmedia.usercrudproject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Marc
 */
@RestController
@RequestMapping("/api/v1/users")
public class RestControllerExample {

    @Autowired
    CustomerRepository userRepository;

    @GetMapping("/all")
    public List<Customer> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Finds all users with a firstname, equal or shorter than the given parameter length.
     * @param shortorequals
     * @return
     */
    @RequestMapping(value = "", params = {
            "shortorequals",
            "limit"
    })
    public List<Customer> getUsersShorterFirstname(
            @RequestParam("shortorequals") int shortorequals,
            @RequestParam("limit") int limit) {
        return userRepository.findAll().stream()
                .filter(user -> !(user.getFirstname().length() >= shortorequals))
                .limit(limit)
                .collect(Collectors.toList());
    }

    /**
     * Find all users width a firstname, equal or longer than the given parameter length.
     * @param longorequals
     * @return
     */
    @RequestMapping(value = "", params = {
            "longorequals",
            "limit"
    })
    public List<Customer> getUsersLongerFirstname(
            @RequestParam("longorequals") int longorequals,
            @RequestParam("limit") int limit) {
        return userRepository.findAll().stream()
                .filter(user -> user.getFirstname().length() >= longorequals)
                .limit(limit)
                .collect(Collectors.toList());
    }



}
