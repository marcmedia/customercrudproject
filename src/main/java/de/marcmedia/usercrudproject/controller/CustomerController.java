package de.marcmedia.usercrudproject.controller;

import de.marcmedia.usercrudproject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;



/**
 * @author Marc
 */
@Controller
@RequestMapping("customers")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/test")
    public String test(Model model) {
        System.out.println(customerRepository.findAll());
        model.addAttribute("customerList", customerRepository.findAll());
        return "index";
    }

    @GetMapping("/{userId}")
    public String displayUser(@PathVariable int userId) {
        return "UserId: " + userId;
    }

    @GetMapping("/createUser")
    public String createUser() {
        return "createUser";
    }

    @GetMapping("/deleteUser")
    public String deleteUser() {
        return null;
    }
}
