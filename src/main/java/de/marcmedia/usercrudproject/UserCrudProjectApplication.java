package de.marcmedia.usercrudproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserCrudProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserCrudProjectApplication.class, args);
    }

}
